// console.log("Hello World");


/* 
    1. Create a switch case statement that will accept the input month number from a user and will print the total number of days in that month. If the input month number is not valid display an alert: "Invalid input! Please enter the month number between 1-12."

    Sample Output:
    - Using prompt to accept user input
    "Enter month number: 3"


    Browser console:
    Total number of days for March: 31

    Hint: Use a parseInt() method to convert the user input to a number.
*/

let monthInNumber = Number(prompt("Enter month number:"));

let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]


switch (monthInNumber) {
    case 1:
        console.log(`Total number of days for ${months[0]}: 31`);
        break;
    case 2:
        console.log(`Total number of days for ${months[1]}: 28`);
        break;
    case 3:
        console.log(`Total number of days for ${months[2]}: 31`);
        break;
    case 4:
        console.log(`Total number of days for ${months[3]}: 30`);
        break;
    case 5:
        console.log(`Total number of days for ${months[4]}: 31`);
        break;
    case 6:
        console.log(`Total number of days for ${months[5]}: 30`);
        break;
    case 7:
        console.log(`Total number of days for ${months[6]}: 31`);
        break;
    case 8:
        console.log(`Total number of days for ${months[7]}: 31`);
        break;
    case 9:
        console.log(`Total number of days for ${months[8]}: 30`);
        break;
    case 10:
        console.log(`Total number of days for ${months[9]}: 31`);
        break;
    case 11:
        console.log(`Total number of days for ${months[10]}: 30`);
        break;
    case 12:
        console.log(`Total number of days for ${months[11]}: 31`);
        break;
    default:
        alert("Invalid input! Please enter the month number between 1-12.");
};

// ==================================================================

/* 
    2. Create a function that will accept the name of the user and will print each character of the name in reverse order. If the length of the name is more than seven characters, the consonants of the name will only be printed in the console.

    Sample Output:
    -User Input
    -"Enter your name:" jane

    Browser console output:
    e
    n
    a
    j

    -User input
    -"Enter your name:" angelito

    Browser console ouput:
    t
    l
    g
    n

*/

// your code here ~~

function displayNameCharacter(){
    let name = prompt("Enter your name:");
    let consonants = "";
    if (name.length <= 7) {
        for (let i = name.length-1; i >= 0 ; i--){
            console.log(name[i]);
        }
    }
    else if (name.length > 7)
    {
        for (let i = name.length-1; i >= 0; i--){
            if (name[i] === "a" ||
                name[i] === "e" ||
                name[i] === "i" ||
                name[i] === "o" ||
                name[i] === "u" )
            {
                continue;
            }
            else {
                consonants += name[i];
                console.log(name[i]);
            }
        }
    }
}

// =================================================================

/* 
    3. Create an array of students with the following value.

        let students = ["John", "Jane", "Joe"];

    Find the following information using array methods: mutators and accessors:
        a. Total size of an array.
        b. Adding a new element at the beginning of an array. (Add: Jack)
        c. Adding a new element at the end of an array. (Add: Jill)
        d. Removing an existing element at the beginning of an array.
        e. Removing an existing element at the end of an array.
        f. Find the index number of the last element in an array.
        g. Print each element of the array in the browser console.

*/

// your code here ~~

let students = ["John", "Jane", "Joe"];

students.length; // a.
console.log(students.length); // returns 3

students.unshift("Jack"); // b.
console.log(students); // added Jack at the beginning of the array

students.push("Jill"); // c.
console.log(students); // added Jill at the end of the array

students.shift(); // d.
console.log(students); // removed Jack from the array

students.pop(); // e.
console.log(students); //removed Jill from the array

console.log(students.indexOf("Joe")); // f. // returns 2

// display every element of the array in the console
for (let i = 0; i < students.length; i++){
    console.log(students[i]);   
};





// =================================================================

/* 
    4. Using the students array, create a function that will look for the index of a given student and will remove the name of the student from the array list.

        Expected Output
        - Initial content of array:
        ["John", "Jane", "Joe"]

        After finding the index and removing the student "Jane".
        ["John", "Joe"]

*/

// your code here ~~

students = ["John", "Jane", "Joe"]; // copy of the initialization above

// function to look the index number of a specific student and remove the specific student from the array
function findIndexOfStudent(student){
    let intendedStudent = students.indexOf(student); // returns a number

    // returns the removed element in the array
    let removedStudent = students.splice(intendedStudent, 1); //start from indexOf specific student and delete 1 element

    console.log(intendedStudent); // returns a number
    console.log(removedStudent); // return the removed element of the array
    console.log(students); // show the array variable with its new value

}





// ====================================================================

/* 
    5. Create a 'person' object that contains properties first name, last name, nick name, age, address(contains city and country), friends(with at least 3 friends with the first name), and a function property displaying text about self introduction about name, age, address, and who is his/her friends.

    Expected output:
    My name is Juan Dela Cruz, but you can call me J. I am 25 years old and I live in Quezon City, Philippines. My friends are John, Jane, and Joe.

    Note: The output will be display through the function property inside the object variable. You may check about this keyword to solve the output.

*/


// your code here ~~

const person = {
    firstName: "Juan",
    nickName: "J",
    lastName: "Dela Cruz",
    age: 25,
    friends: ["John", "Jane", "Joe"],
    address: {
        city: "Quezon City",
        country: "Philippines"
    },
    introduce: function(){
        console.log(`My name is ${person.firstName} ${person.lastName}, but you can call me ${person.nickName} I am ${person.age} years old and I live in ${person.address.city}, ${person.address.country}. My friends are ${person.friends[0]}, ${person.friends[1]}, and ${person.friends[2]}.`);
    }
}

person.introduce();



// ======================================================================

/* 
    6. Create another copy of the 'person' object use in the previous activity and apply the following ES6 updates:
        a. Apply the JavaScript Destructuring Assignment in the object variable.
        b. Use JavaScript Template Literals in displaying the text inside the function property of the object.
        c. Run the program and check if it still produce the expected output.

*/


// your code here ~~

const person2 = {
    firstName: "Juan",
    nickName: "J",
    lastName: "Dela Cruz",
    age: 25,
    friends: ["John", "Jane", "Joe"],
    address: {
        city: "Quezon City",
        country: "Philippines"
    },
    introduce: function () {
        console.log(`My name is ${person.firstName} ${person.lastName}, but you can call me ${person.nickName} I am ${person.age} years old and I live in ${person.address.city}, ${person.address.country}. My friends are ${person.friends[0]}, ${person.friends[1]}, and ${person.friends[2]}.`);
    }
}

const {firstName, nickName, lastName, age, friends, address,} = person2;

console.log(`My name is ${firstName} ${lastName}, but you can call me ${nickName} I am ${age} years old and I live in ${address.city}, ${address.country}. My friends are ${friends[0]}, ${friends[1]}, and ${friends[2]}.`);